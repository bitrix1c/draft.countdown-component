# Draft.CountDown Component

Пример подключения компонента:

 ```
 $APPLICATION->IncludeComponent(
        "hardkod:draft.countdown",
        "",
        [
            "TYPE" => "to", // или from
            "FROM_TIME" => date("d.m.Y H.i.s"),
            "TO_TIME" => date("d.m.Y H.i.s", $timestamp),
            "TIME" => 86400 * 5, // актуально если TYPE => from
            "TITLE" => "До конца распродажи осталось", // заголовок над счётчиком
            ],
        false
 );
```
---