<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/*
 *  @package     Draft.CountDown Component
 *  @author      Anton Egorov (ye.anton@ya.ru)
 *  @copyright   (C) 2021. All rights reserved.
 *  @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html
 *  @link        https://gitlab.com/bitrix1c/draft.countdown-component
 *
 *  Based on class CBitrixComponent  from 1C Bitrix!
 *
 *
 */

class DraftCountdownComponent extends \CBitrixComponent {

    public function executeComponent()
    {

        $arTime = [];
        $now = new \DateTime();

        if($this->arParams["TYPE"] == "to"){
            $timeObj = $now->diff((new \DateTime($this->arParams["TO_TIME"])));
            //передаём значение в шаблон в виде массива Дни, Часы, Минуты, Секунды
            if(!$timeObj->invert){
                $arTime = [
                    "days" => $timeObj->d,
                    "hours" => $timeObj->h,
                    "minutes" => $timeObj->i,
                    "seconds" => $timeObj->s,
                ];
            }

        }elseif ($this->arParams["TYPE"] == "from"){
            // получаем разницу между параметрами FROM_TIME и TIME и передать
            // значение в шаблон в виде массива Дни, Часы, Минуты, Секунды
            $from = new \DateTime($this->arParams["FROM_TIME"]);
            $time = $from->getTimestamp() + $this->arParams["TIME"];
            $timeObj = $now->diff((new \DateTime())->setTimestamp($time));
//            $this->arResult["from"] = $from;
//            $this->arResult["diff"] = $timeObj;
//            $this->arResult["time"] = (new \DateTime())->setTimestamp($time);

            if(!$timeObj->invert){
                $arTime = [
                    "days" => $timeObj->d,
                    "hours" => $timeObj->h,
                    "minutes" => $timeObj->i,
                    "seconds" => $timeObj->s,
                ];
            }
        }

        $this->arResult = array_merge($this->arResult, $arTime);

        $this->includeComponentTemplate();
    }

    public function onPrepareComponentParams($arParams)
    {
        if(!isset($arParams["TYPE"]) || empty($arParams["TYPE"])){
            $arParams["TYPE"] = "from";
        }
        if(!isset($arParams["TO_TIME"]) || empty($arParams["TO_TIME"])){
            $arParams["TO_TIME"] = date("d.m.Y");
        }
        if(!isset($arParams["FROM_TIME"]) || empty($arParams["FROM_TIME"])){
            $arParams["FROM_TIME"] = date("d.m.Y");
        }
        if(!isset($arParams["TIME"]) || empty($arParams["TIME"])){
            $arParams["TIME"] = "3600";
        }
        return $arParams;
    }

    public function pluralize($number, $words) {
        $cases = [2, 0, 1, 1, 1, 2];
        $n = $number;
        return sprintf($words[ ($n%100>4 && $n%100<20) ? 2 : $cases[min($n%10, 5)] ], $n);
    }

}