<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/*
 *  @package     Draft.CountDown Component
 *  @author      Anton Egorov (ye.anton@ya.ru)
 *  @copyright   (C) 2021. All rights reserved.
 *  @license     http://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html
 *  @link        https://gitlab.com/bitrix1c/draft.countdown-component
 *
 *  Based on class CBitrixComponent  from 1C Bitrix!
 *
 *
 */

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalJs("https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js");
?>
<!--DRAFT COUNTDOUWN-->
<!--<pre>-->
<!--    --><?//var_dump($arResult)?>
<!--</pre>-->
<div class="countdown__wrapper">
        <span class="countdown__title"><?=$arParams["TITLE"]?:"Обратный отсчёт"?>:</span>
    <div class="countdown">
        <div class="bloc-time days" data-init-value="<?=$arResult["days"]?>">
            <span class="count-title">Дни</span>

            <div class="figure days days-1">
                <span class="top">0</span>
                <span class="top-back"><span>0</span></span>
                <span class="bottom">0</span>
                <span class="bottom-back"><span>0</span></span>
            </div>

            <div class="figure days days-2">
                <span class="top">0</span>
                <span class="top-back"><span>0</span></span>
                <span class="bottom">0</span>
                <span class="bottom-back"><span>0</span></span>
            </div>
        </div>

        <div class="bloc-time hours" data-init-value="<?=$arResult["hours"]?>">
            <span class="count-title">Часы</span>

            <div class="figure hours hours-1">
                <span class="top">0</span>
                <span class="top-back"><span>0</span></span>
                <span class="bottom">0</span>
                <span class="bottom-back"><span>0</span></span>
            </div>

            <div class="figure hours hours-2">
                <span class="top">0</span>
                <span class="top-back"><span>0</span></span>
                <span class="bottom">0</span>
                <span class="bottom-back"><span>0</span></span>
            </div>
        </div>

        <div class="bloc-time min" data-init-value="<?=$arResult["minutes"]?>">
            <span class="count-title">Минуты</span>

            <div class="figure min min-1">
                <span class="top">0</span>
                <span class="top-back"><span>0</span></span>
                <span class="bottom">0</span>
                <span class="bottom-back"><span>0</span></span>
            </div>

            <div class="figure min min-2">
                <span class="top">0</span>
                <span class="top-back"><span>0</span></span>
                <span class="bottom">0</span>
                <span class="bottom-back"><span>0</span></span>
            </div>
        </div>

        <div class="bloc-time sec" data-init-value="<?=$arResult["seconds"]?>">
            <span class="count-title">Секунды</span>

            <div class="figure sec sec-1">
                <span class="top">0</span>
                <span class="top-back"><span>0</span></span>
                <span class="bottom">0</span>
                <span class="bottom-back"><span>0</span></span>
            </div>

            <div class="figure sec sec-2">
                <span class="top">0</span>
                <span class="top-back"><span>0</span></span>
                <span class="bottom">0</span>
                <span class="bottom-back"><span>0</span></span>
            </div>
        </div>
    </div>
</div>
